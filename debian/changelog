r-cran-openmx (2.21.13+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Charles Plessy <plessy@debian.org>  Tue, 28 Jan 2025 16:36:07 +0900

r-cran-openmx (2.21.12+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Charles Plessy <plessy@debian.org>  Thu, 03 Oct 2024 10:05:00 +0900

r-cran-openmx (2.21.11+dfsg-4) unstable; urgency=medium

  * Team upload.
  * Rebuild against latest Matrix (closes: #1070239)

 -- Charles Plessy <plessy@debian.org>  Tue, 07 May 2024 09:43:19 +0900

r-cran-openmx (2.21.11+dfsg-3) unstable; urgency=medium

  * Fix regexp to detect r-base-core version in case of binary only NMUs
    Closes: #1069388
  * Standards-Version: 4.7.0 (routine-update)

 -- Andreas Tille <tille@debian.org>  Wed, 24 Apr 2024 09:55:55 +0200

r-cran-openmx (2.21.11+dfsg-2) unstable; urgency=medium

  * Switch to default version of boost
    Closes: #1060792

 -- Andreas Tille <tille@debian.org>  Mon, 15 Jan 2024 08:06:08 +0100

r-cran-openmx (2.21.11+dfsg-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Tue, 09 Jan 2024 08:03:40 +0100

r-cran-openmx (2.21.10+dfsg-1) unstable; urgency=medium

  * Add dependency from r-cran-matrix version it was built against
    (see https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1055922#68)
  * New upstream version
  * dh-update-R to update Build-Depends (routine-update)

 -- Andreas Tille <tille@debian.org>  Sun, 19 Nov 2023 18:32:16 +0100

r-cran-openmx (2.21.8+dfsg-1) unstable; urgency=medium

  * New upstream version
  * dh-update-R to update Build-Depends (routine-update)
  * Sync Test-Depends with Suggests
  * Versioned Test-Depends: r-cran-ggplot2 (>= 3.4.2+dfsg-1~) to deal with
    R graphics ABI change
  * Build-Depend from libboost-system1.81-dev to avoid conflict between
    r-cran-bh and libboost-system-dev (FIXME: This needs to be reverted
    once libboost 1.81 is default.)
    Closes: #1042178
  * Lintian-override for custom-library-search-path

 -- Andreas Tille <tille@debian.org>  Fri, 18 Aug 2023 09:36:46 +0200

r-cran-openmx (2.21.1+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)
  * lintian-overrides (see lintian bug #1017966)

 -- Andreas Tille <tille@debian.org>  Fri, 27 Jan 2023 09:32:58 +0100

r-cran-openmx (2.20.7+dfsg-1) unstable; urgency=medium

  * Team Upload.
  * New upstream version 2.20.7+dfsg

 -- Nilesh Patra <nilesh@debian.org>  Sun, 23 Oct 2022 19:28:36 +0530

r-cran-openmx (2.20.6+dfsg-2) unstable; urgency=medium

  * Team Upload.
  * Re-build with rcppparallel
  * Bump Standards-Version to 4.6.1 (no changes needed)

 -- Nilesh Patra <nilesh@debian.org>  Mon, 12 Sep 2022 22:29:58 +0530

r-cran-openmx (2.20.6+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Testsuite: autopkgtest-pkg-r (routine-update)

 -- Andreas Tille <tille@debian.org>  Wed, 16 Mar 2022 11:43:24 +0100

r-cran-openmx (2.20.3+dfsg-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Sat, 12 Feb 2022 19:20:07 +0100

r-cran-openmx (2.20.0+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Remove documentation that includes compressed JS

 -- Andreas Tille <tille@debian.org>  Mon, 24 Jan 2022 21:24:35 +0100

r-cran-openmx (2.19.8-2) unstable; urgency=medium

  * Team Upload.
  [ Andreas Tille ]
  * Disable reprotest

  [ Nilesh Patra ]
  * d/t/run-unit-test: Exclude more tests on i386 and ppc64el

 -- Nilesh Patra <nilesh@debian.org>  Mon, 04 Oct 2021 00:55:42 +0530

r-cran-openmx (2.19.8-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)
  * dh-update-R to update Build-Depends (routine-update)
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on r-cran-bh.

 -- Andreas Tille <tille@debian.org>  Wed, 15 Sep 2021 17:26:52 +0200

r-cran-openmx (2.18.1-3) unstable; urgency=high

  * Exclude another test for i386 and skip test on armhf fully
    Closes: #971681

 -- Andreas Tille <tille@debian.org>  Wed, 10 Feb 2021 08:38:26 +0100

r-cran-openmx (2.18.1-2) unstable; urgency=medium

  * Exclude tests that are not working on all architectures
  * Standards-Version: 4.5.1 (routine-update)

 -- Andreas Tille <tille@debian.org>  Tue, 09 Feb 2021 17:51:11 +0100

r-cran-openmx (2.18.1-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Thu, 03 Sep 2020 12:56:12 +0200

r-cran-openmx (2.17.4-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * debhelper-compat 13 (routine-update)

 -- Dylan Aïssi <daissi@debian.org>  Thu, 16 Jul 2020 10:17:19 +0200

r-cran-openmx (2.17.3-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Rules-Requires-Root: no (routine-update)

 -- Dylan Aïssi <daissi@debian.org>  Mon, 06 Apr 2020 14:18:08 +0200

r-cran-openmx (2.17.2-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.5.0 (routine-update)

 -- Andreas Tille <tille@debian.org>  Wed, 19 Feb 2020 19:37:27 +0100

r-cran-openmx (2.15.5-1) unstable; urgency=medium

  * New upstream version
  * Try to prevent dh-update-R from removal of python3 from Depends
  * Set upstream metadata fields: Repository.

 -- Andreas Tille <tille@debian.org>  Mon, 06 Jan 2020 10:42:36 +0100

r-cran-openmx (2.15.4-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Wed, 27 Nov 2019 16:52:22 +0100

r-cran-openmx (2.15.3-1) unstable; urgency=medium

  * New upstream version
  * Set upstream metadata fields: Archive, Bug-Database, Repository.

 -- Andreas Tille <tille@debian.org>  Wed, 20 Nov 2019 09:33:51 +0100

r-cran-openmx (2.14.11-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.4.1
  * Depends: python3
  * Add autopkgtest

 -- Andreas Tille <tille@debian.org>  Tue, 12 Nov 2019 13:30:51 +0100

r-cran-openmx (2.13.2+dfsg-1) unstable; urgency=medium

  * Initial release (closes: #940653)

 -- Andreas Tille <tille@debian.org>  Wed, 18 Sep 2019 18:28:30 +0200
